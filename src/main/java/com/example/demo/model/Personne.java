package com.example.demo.model;

import lombok.experimental.FieldDefaults;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

//@FieldDefaults(level= AccessLevel.PRIVATE)
//@Setter
//@Getter
//@ToString(of = {"nom", "prenom"})
@Data
@AllArgsConstructor
@NoArgsConstructor
//@RequiredArgsConstructor
public class Personne {
	Integer num;
	@NonNull
	String nom;
	String prenom;
}
