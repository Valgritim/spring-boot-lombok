package com.example.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.example.demo.model.Personne;

@Controller
public class LombokController {
	
	@GetMapping(value="/lombok")
	public void sayHello() {
	
		Personne personne = new Personne(1,"Musk","Elon");
		System.out.println(personne);

		
	}
}
